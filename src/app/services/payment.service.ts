import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Payment } from '../models/payment.model';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  baseUrl:string = '';

  constructor(private http:HttpClient) { }

  confirmPayment(payment:Payment):Observable<boolean>{
    try{
      this.http.post(this.baseUrl,payment);
    }catch{
      
    }
    return of(true);
  }
}
