export interface Payment {
    cardNumber: string;
    cardHolder: string;
    cardExpiry: Date;
    cardCVV: string;
    amount: number;
}