import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { AppState } from '../app.state';
import { Payment } from '../models/payment.model';
import { PaymentService } from '../services/payment.service';
import * as PaymentActions from './../actions/payment.actions';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  paymentForm:FormGroup;

  constructor(private paymentService:PaymentService, private toastr: ToastrService, private router: Router, private store: Store<AppState>) { }

  ngOnInit(): void {
    this.createPaymentForm();
  }

  createPaymentForm(){
    this.paymentForm = new FormGroup({
      cardHolder: new FormControl('', Validators.required),
      cardNumber: new FormControl('', [Validators.required, Validators.pattern("^\\d{16}$")]),
      cardExpiry: new FormControl('', [Validators.required,Validators.pattern("^(0[123456789]|10|11|12)([/])([1-2][0-9][0-9][0-9])$")]),
      cardCVV: new FormControl('', [Validators.maxLength(3), Validators.minLength(3)]),
      amount: new FormControl('', [Validators.required, Validators.min(1)])
    })
  }

  get cardHolder(){
    return this.paymentForm.get('cardHolder');
  }

  get cardNumber(){
    return this.paymentForm.get('cardNumber');
  }

  get cardExpiry(){
    return this.paymentForm.get('cardExpiry');
  }

  get cardCVV(){
    return this.paymentForm.get('cardCVV');
  }

  get amount(){
    return this.paymentForm.get('amount');
  }

  validateExpiryDate(cardExpiry:string):boolean  {
    let month = Number(cardExpiry.substring(0,2));
    let year = Number(cardExpiry.substring(3,7));

    let today = new Date();
    let mm = today.getMonth()+1; 
    let yyyy = today.getFullYear();

    if(year<yyyy){
      return false;
    }
    if(year == yyyy && month<mm){
      return false;
    }
    return true;
  }

  convertToDate(cardExpiry):Date{
 
    let day = '01';
    let month = cardExpiry.substring(0,2);
    let year = cardExpiry.substring(3,7);
    let date: Date = new Date(year+'-'+month+'-'+day);
    console.log(date);
    return date;
  }

  onSubmit(){
    console.log(this.paymentForm.value);
    let payment : {
      card
    }
    if(this.validateExpiryDate(this.cardExpiry.value)){

      let payment: Payment={
        cardNumber:this.cardNumber.value,
        cardHolder:this.cardHolder.value,
        cardExpiry:this.convertToDate(this.cardExpiry.value),
        cardCVV:this.cardCVV.value,
        amount:this.amount.value
      };

      this.paymentService.confirmPayment(payment).subscribe(result =>{
        if(result)
        {
          //display toast and route to dashboard
          this.store.dispatch(new PaymentActions.AddPayment({cardHolder:this.paymentForm.get('cardHolder').value, cardNumber:this.paymentForm.get('cardNumber').value, cardExpiry:this.convertToDate(this.cardExpiry.value), cardCVV:this.paymentForm.get('cardCVV').value, amount:this.paymentForm.get('amount').value}));
          this.toastr.success('amount was transferred successfully','Payment Successful');
          this.router.navigate(['/dashboard']);
        }
      },error =>{
        console.log(error);
      });
    }else{
      this.toastr.error('card has expired','Payment Failed');
    }
    
  }

}
