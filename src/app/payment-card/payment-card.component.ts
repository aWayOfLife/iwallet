import { Component, Input, OnInit } from '@angular/core';
import { Payment } from '../models/payment.model';

@Component({
  selector: 'app-payment-card',
  templateUrl: './payment-card.component.html',
  styleUrls: ['./payment-card.component.css']
})
export class PaymentCardComponent implements OnInit {
  @Input() payment:Payment;
  constructor() { }

  ngOnInit(): void {
  }

}
