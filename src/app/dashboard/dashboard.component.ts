import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Payment } from '../models/payment.model';
import { AppState } from './../app.state';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  payments: Observable<Payment[]>;
  recentPayments: Payment[];

  constructor(private store: Store<AppState>) { 
    this.payments = store.select('payment');
    //console.log(this.payments);
  }

  ngOnInit(): void {
    this.payments.subscribe(result =>{
      this.recentPayments = result;
      console.log(result);
    })
  }

}
